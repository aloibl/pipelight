#
# Enables a diagnostic mode which might be helpful to find an
# error in the configuration or installation.
# To get the error messages go to: http://fds-team.de/pipelight
#
# Information for advanced users: The diagnostic page embeds the plugin
# of type "application/x-pipelight-error" to trigger the output of some
# debug information. The plugin cannot be triggered if everything is
# working, so this only affects users with a broken installation.
# The debug output will include paths on the local filesystem and the
# linux distribution used. If you don't want to leak this information
# accidentially in case of a broken installation please either uninstall
# Pipelight or disable the diagnosticMode. [default: false]
#
diagnosticMode      = true

#
# Path to the wine directory
# * the wine executable itself should be at: winePath/bin/wine
# * the winepath executable should be at:    winePath/bin/winepath
#
# Please note: In older versions this was a path to the wine
# executable instead. [deprecated, but should still work]
#
winePath            = /opt/wine-compholio/

#
# Path to the wine prefix containing Silverlight
#
winePrefix          = $HOME/.wine-pipelight/

#
# The wine architecture for the wine prefix containing Silverlight
#
wineArch            = win32

#
# DLLs to overwrite in Wine
# (prevents Wine from asking for Gecko, Mono or winegstreamer)
#
wineDLLOverrides    = mscoree,mshtml,winegstreamer=

#
# Path to the plugin loader executable
# (Should be set correctly by the make script)
#
pluginLoaderPath    = PLUGIN_LOADER_PATH

#
# Path to the runtime DLLs (libgcc_s_sjlj-1.dll, libspp-0.dll,
# libstdc++-6.dll). Only necessary when these DLLs are not in the same
# directory as the pluginloader executable.
#
gccRuntimeDlls      = /usr/lib/gcc/i686-w64-mingw32/4.6/

#
# Path to the Silverlight directory
# Must be changed if a different version should be used.
# The standard installation Path is:
# c:\Program Files\Microsoft Silverlight\5.1.20513.0\
#
dllPath             = c:\Program Files\Silverlight\5.1.20513.0\

#
# Name of the Silverlight library to load
# (relative to the path specified above)
#
dllName             = npctrl.dll

#
# fakeVersion allows to fake the version string of Silverlight
# Allows to get around some version checks done by some websites
# when using an old version of Silverlight.
#
# fakeVersion       = 5.1.20513.0

#
# overwriteArg allows to overwrite/add initialization arguments
# passed by websites to Silverlight applications. You can
# use this option as often as you want to overwrite multiple
# parameters. The GPU acceleration state of Silverlight can be controlled
# by setting:
#
#   enableGPUAcceleration=false     # disable GPU acceleration
#   comment out                     # let the application decide (default)
#   enableGPUAcceleration=true      # force GPU acceleration
#
# You may need to overwrite the minimum runtime version if
# you use an old Silverlight version as some websites set
# an artificial limit for the version number although it 
# would work with older versions.
#
# overwriteArg      = minRuntimeVersion=5.0.61118.0
# overwriteArg      = enableGPUAcceleration=false
# overwriteArg      = enableGPUAcceleration=true
#

#
# windowlessmode refers to a term of the Netscape Plugin API and 
# defines a different mode of drawing and handling events.
# On some desktop enviroments you may have problems using the
# keyboard in windowless mode, on the other hand the drawing is
# more efficient when this mode is enabled. Just choose what works
# best for you. [default: false]
#
windowlessMode      = false

#
# embed defines whether the Silverlight plugin should be shown 
# inside the browser (true) or an external window (false).
# [default: true]
#
embed               = true

#
# Path to the dependency installer script provided by the compholio
# package. (optional)
#
dependencyInstaller = /usr/share/wine-browser-installer/install-dependency

#
# Silverlight version to install using the dependencyInstaller
# The argument will be passed as "wine-(silverlightVersion)-installer"
# (optional)
#
# Valid values are:
#   silverlight5.1
#   silverlight5.0
#   silverlight4
#
# If you changes this value, you also need to adjust dllPath
#
silverlightVersion  = silverlight5.1

#
# Additional dependency arguments which should be passed to the
# dependencyInstaller, can be used multiple times. (optional)
#
dependency          = wine-mpg2splt-installer

#
# In order to support browsers without NPAPI timer support
# (like Midori) we've implemented a fallback to
# NPN_PluginThreadAsyncCall. In the default configuration 
# a timer based approach is preferred over async calls and the
# plugin decides by itself which method to use depending on the
# browser capabilities. Setting the following option to true
# forces the plugin to use async calls. This might be mainly
# useful for testing the difference between both event handling
# approaches. [default: false]
#
# eventAsyncCall    = true

#
# The opera browser claims to provide timer functions, but they
# don't seem to work properly. When the opera detection is
# enabled Pipelight will switch to eventAsyncCall automatically
# based on the user agent string. [default: true]
#
operaDetection      = true

#
# Minimal JavaScript user agent switcher. If your page doesn't check
# the user agent before loading a Silverlight instance, you can use
# this trick to overwrite the useragent or execute any other Java-
# Script you want. You can use this command multiple times.
# Uncomment the following 4 lines for FF15 spoofing.
#
# executejavascript = var __originalNavigator = navigator;
# executejavascript = navigator = new Object();
# executejavascript = navigator.__proto__ = __originalNavigator;
# executejavascript = navigator.__defineGetter__('userAgent', function () { return 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1'; });

#
# We are currently implementing hardware acceleration support, which
# can cause problems on some systems as especially specific open source
# drivers render only garbage when hardware acceleration is enabled.
# To prevent breaking any working solutions, we are currently
# implementing a whitelist system, which will enable hardware
# acceleration by default if the specified shell scripts returns 0.
# Otherwise we will disable it. You can still use
#
# overwriteArg      = enableGPUAcceleration=false/true
#
# to overwrite the check results. If you really want to skip this test
# you can use: graphicDriverCheck = /bin/true
#
graphicDriverCheck  = /usr/share/wine-browser-installer/hw-accel-default

#------------------------- EXPERIMENTAL -------------------------
# Watch out: The following section contains highly experimental
# stuff! These functions are likely not working properly yet and
# might be removed at any time.

#
# Silverlight uses a lot of timer stuff do to the window redrawing
# . In order to speed this up a bit the following switch enables
# some API hooks to do most of timer stuff in user mode (without
# having to call wine-server each time). It is still unclear
# if this option has any significant effent on the performance.
# [default: false]
#
# experimental-userModeTimer = true
