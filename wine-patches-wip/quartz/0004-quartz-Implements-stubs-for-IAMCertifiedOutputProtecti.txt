From 0bc1f85938325657c8237df3758bd145e27c2148 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Michael=20M=C3=BCller?= <michael@fds-team.de>
Date: Sat, 31 Aug 2013 07:05:12 +0200
Subject: quartz: Implements stubs for IAMCertifiedOutputProtection

---
 dlls/quartz/vmr9.c |  127 ++++++++++++++++++++++++++++++++++++++++++++++++++++
 include/errors.h   |    1 +
 include/vmr9.idl   |   41 +++++++++++++++++
 3 files changed, 169 insertions(+)

diff --git a/dlls/quartz/vmr9.c b/dlls/quartz/vmr9.c
index 095ea50..d154a28 100644
--- a/dlls/quartz/vmr9.c
+++ b/dlls/quartz/vmr9.c
@@ -181,8 +181,20 @@ static inline IVMRMonitorConfigImpl *impl_from_IVMRMonitorConfig( IVMRMonitorCon
     return CONTAINING_RECORD(iface, IVMRMonitorConfigImpl, IVMRMonitorConfig_iface);
 }
 
+typedef struct
+{
+    IAMCertifiedOutputProtection IAMCertifiedOutputProtection_iface;
+    LONG refCount;
+} IAMCertifiedOutputProtectionImpl;
+
+static inline IAMCertifiedOutputProtectionImpl *impl_from_IAMCertifiedOutputProtection( IAMCertifiedOutputProtection *iface)
+{
+    return CONTAINING_RECORD(iface, IAMCertifiedOutputProtectionImpl, IAMCertifiedOutputProtection_iface);
+}
+
 static HRESULT VMR9DefaultAllocatorPresenterImpl_create(VMR9Impl *parent, LPVOID * ppv);
 static HRESULT IVMRMonitorConfigImpl_create(LPVOID * ppv);
+static HRESULT IAMCertifiedOutputProtection_create(LPVOID * ppv);
 
 static DWORD VMR9_SendSampleData(VMR9Impl *This, VMR9PresentationInfo *info, LPBYTE data, DWORD size)
 {
@@ -781,6 +793,13 @@ static HRESULT WINAPI VMR9Inner_QueryInterface(IUnknown * iface, REFIID riid, LP
         if (SUCCEEDED(hr))
             return hr;
     }
+    else if (IsEqualIID(riid, &IID_IAMCertifiedOutputProtection))
+    {
+        HRESULT hr;
+        hr = IAMCertifiedOutputProtection_create(ppv);
+        if (SUCCEEDED(hr))
+            return hr;
+    }
     else if (IsEqualIID(riid, &IID_IVMRMixerControl9))
         FIXME("No interface for IID_IVMRMixerControl9\n");
     else
@@ -2447,6 +2466,114 @@ static HRESULT IVMRMonitorConfigImpl_create(LPVOID *ppv)
     return S_OK;
 }
 
+static HRESULT WINAPI VMR_IAMCertifiedOutputProtection_QueryInterface(IAMCertifiedOutputProtection *iface, REFIID riid, LPVOID *ppv)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+    TRACE("(%p/%p)->(%s, %p)\n", This, iface, qzdebugstr_guid(riid), ppv);
+
+    *ppv = NULL;
+
+    if (IsEqualIID(riid, &IID_IUnknown))
+        *ppv = (LPVOID)&(This->IAMCertifiedOutputProtection_iface);
+    else if (IsEqualIID(riid, &IID_IAMCertifiedOutputProtection))
+        *ppv = &This->IAMCertifiedOutputProtection_iface;
+
+    if (*ppv)
+    {
+        IUnknown_AddRef((IUnknown *)(*ppv));
+        return S_OK;
+    }
+
+    FIXME("No interface for %s\n", debugstr_guid(riid));
+
+    return E_NOINTERFACE;
+}
+
+static ULONG WINAPI VMR_IAMCertifiedOutputProtection_AddRef(IAMCertifiedOutputProtection *iface)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+    ULONG refCount = InterlockedIncrement(&This->refCount);
+
+    TRACE("(%p)->() AddRef from %d\n", iface, refCount - 1);
+
+    return refCount;
+}
+
+static ULONG WINAPI VMR_IAMCertifiedOutputProtection_Release(IAMCertifiedOutputProtection *iface)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+    ULONG refCount = InterlockedDecrement(&This->refCount);
+
+    TRACE("(%p)->() Release from %d\n", iface, refCount + 1);
+
+    if (!refCount)
+    {
+        TRACE("Destroying\n");
+        CoTaskMemFree(This);
+        return 0;
+    }
+    return refCount;
+}
+
+static HRESULT WINAPI VMR_IAMCertifiedOutputProtection_KeyExchange(IAMCertifiedOutputProtection *iface, GUID *pRandom, BYTE **VarLenCertGH, DWORD *pdwLengthCertGH)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+
+    FIXME("(%p/%p)->(...)\n stub", iface, This);
+    return VFW_E_NO_COPP_HW;
+}
+
+static HRESULT WINAPI VMR_IAMCertifiedOutputProtection_SessionSequenceStart(IAMCertifiedOutputProtection *iface, AMCOPPSignature *pSig)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+
+    FIXME("(%p/%p)->(...)\n stub", iface, This);
+    return VFW_E_NO_COPP_HW;
+}
+
+static HRESULT WINAPI VMR_IAMCertifiedOutputProtection_ProtectionCommand(IAMCertifiedOutputProtection *iface, const AMCOPPCommand *cmd)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+
+    FIXME("(%p/%p)->(...)\n stub", iface, This);
+    return VFW_E_NO_COPP_HW;
+}
+
+static HRESULT WINAPI VMR_IAMCertifiedOutputProtection_ProtectionStatus(IAMCertifiedOutputProtection *iface, const AMCOPPStatusInput *pStatusInput, AMCOPPStatusOutput *pStatusOutput)
+{
+    IAMCertifiedOutputProtectionImpl *This = impl_from_IAMCertifiedOutputProtection(iface);
+
+    FIXME("(%p/%p)->(...)\n stub", iface, This);
+    return VFW_E_NO_COPP_HW;
+}
+
+static const IAMCertifiedOutputProtectionVtbl IAMCertifiedOutputProtection_Vtbl =
+{
+    VMR_IAMCertifiedOutputProtection_QueryInterface,
+    VMR_IAMCertifiedOutputProtection_AddRef,
+    VMR_IAMCertifiedOutputProtection_Release,
+    VMR_IAMCertifiedOutputProtection_KeyExchange,
+    VMR_IAMCertifiedOutputProtection_SessionSequenceStart,
+    VMR_IAMCertifiedOutputProtection_ProtectionCommand,
+    VMR_IAMCertifiedOutputProtection_ProtectionStatus,
+};
+
+static HRESULT IAMCertifiedOutputProtection_create(LPVOID *ppv)
+{
+    IAMCertifiedOutputProtectionImpl* This;
+
+    This = CoTaskMemAlloc(sizeof(IAMCertifiedOutputProtectionImpl));
+    if (!This)
+        return E_OUTOFMEMORY;
+
+    This->IAMCertifiedOutputProtection_iface.lpVtbl = &IAMCertifiedOutputProtection_Vtbl;
+
+    This->refCount = 1;
+
+    *ppv = This;
+    return S_OK;
+}
+
 
 static IDirect3D9 *init_d3d9(HMODULE d3d9_handle)
 {
diff --git a/include/errors.h b/include/errors.h
index c8c3067..58831ed 100644
--- a/include/errors.h
+++ b/include/errors.h
@@ -27,6 +27,7 @@ extern "C" {
 
 #define VFW_FIRST_CODE      0x200
 #define MAX_ERROR_TEXT_LEN  160
+#define VFW_E_NO_COPP_HW    0x8004029b
 
 #include <vfwmsgs.h>
 
diff --git a/include/vmr9.idl b/include/vmr9.idl
index bb3681c..77ed1fc 100644
--- a/include/vmr9.idl
+++ b/include/vmr9.idl
@@ -522,3 +522,44 @@ interface IVMRImageCompositor9 : IUnknown
                            [in] REFERENCE_TIME start, [in] REFERENCE_TIME stop, D3DCOLOR back,
                            [in] VMR9VideoStreamInfo *info, [in] UINT streams);
 };
+
+typedef struct _AMCOPPSignature {
+    BYTE Signature[256];
+} AMCOPPSignature;
+
+typedef struct _AMCOPPCommand {
+    GUID  macKDI;
+    GUID  guidCommandID;
+    DWORD dwSequence;
+    DWORD cbSizeData;
+    BYTE  CommandData[4056];
+} AMCOPPCommand, *LPAMCOPPCommand;
+
+typedef struct _AMCOPPStatusInput {
+    GUID  rApp;
+    GUID  guidStatusRequestID;
+    DWORD dwSequence;
+    DWORD cbSizeData;
+    BYTE  StatusData[4056];
+} AMCOPPStatusInput, *LPAMCOPPStatusInput;
+
+typedef struct _AMCOPPStatusOutput {
+    GUID  macKDI;
+    DWORD cbSizeData;
+    BYTE  COPPStatus[4076];
+} AMCOPPStatusOutput, *LPAMCOPPStatusOutput;
+
+[
+    local,
+    object,
+    uuid(6feded3e-0ff1-4901-a2f1-43f7012c8515),
+    helpstring("IAMCertifiedOutputProtection interface"),
+    pointer_default(unique)
+]
+interface IAMCertifiedOutputProtection : IUnknown
+{
+    HRESULT KeyExchange([out] GUID *pRandom, [out] BYTE **VarLenCertGH, [out] DWORD *pdwLengthCertGH);
+    HRESULT SessionSequenceStart([in] AMCOPPSignature *pSig);
+    HRESULT ProtectionCommand([in] const AMCOPPCommand *cmd);
+    HRESULT ProtectionStatus([in] const AMCOPPStatusInput *pStatusInput, [out] AMCOPPStatusOutput *pStatusOutput);
+};
\ No newline at end of file
-- 
1.7.9.5

